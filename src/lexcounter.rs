use super::crdt::DeltaCrdt;
use super::internal::lex::LexPair;
use std::collections::{BTreeMap, BTreeSet};
use std::hash::Hash;
use std::iter::FromIterator;
use std::ops::Deref;

type LexCounterState<ID> = BTreeMap<ID, LexPair<i64, i64>>;

#[derive(Clone)]
pub struct LexCounter<ID> {
    pub(crate) state: LexCounterState<ID>,
    value: i64,
}

impl<ID: Hash + Eq + Ord + Clone> LexCounter<ID> {
    pub fn new() -> Self {
        LexCounter {
            state: BTreeMap::new(),
            value: 0,
        }
    }

    pub fn increment(&mut self, id: ID) -> LexCounterState<ID> {
        let current = self
            .state
            .get(&id)
            .map(|c| c.to_owned())
            .unwrap_or_else(|| LexPair::new(0, 0, true));

        let mut delta = BTreeMap::new();
        delta.insert(id, LexPair::new(current.value + 1, current.timestamp, true));

        self.update(delta.clone());

        delta
    }

    pub fn decrement(&mut self, id: ID) -> LexCounterState<ID> {
        let current = self
            .state
            .get(&id)
            .map(|c| c.to_owned())
            .unwrap_or_else(|| LexPair::new(0, 0, true));

        let mut delta = BTreeMap::new();
        delta.insert(
            id,
            LexPair::new(current.value - 1, current.timestamp + 1, true),
        );

        self.update(delta.clone());

        delta
    }
}

impl<ID: Hash + Eq + Ord + Clone> DeltaCrdt for LexCounter<ID> {
    type State = LexCounterState<ID>;

    fn update(&mut self, mut theirs: Self::State) {
        let keys: BTreeSet<_> =
            BTreeSet::from_iter(self.state.keys().chain(theirs.keys()).cloned());

        for key in keys {
            if let Some(value) = LexPair::join(
                self.state.remove(&key),
                theirs.remove(&key),
                |ours, theirs| {
                    if ours.value > theirs.value {
                        ours
                    } else {
                        theirs
                    }
                },
            ) {
                self.state.insert(key, value);
            }
        }

        self.derive();
    }

    fn derive(&mut self) {
        self.value = self.state.values().map(|lex_pair| lex_pair.value).sum();
    }
}

impl<ID: Hash + Eq + Ord + Clone> Deref for LexCounter<ID> {
    type Target = i64;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_starts_with_zero() {
        let counter: LexCounter<String> = LexCounter::new();
        assert_eq!(*counter, 0)
    }

    #[test]
    fn it_increments() {
        let mut counter = LexCounter::new();
        counter.increment("alice");
        assert_eq!(*counter, 1);
    }

    #[test]
    fn it_decrements() {
        let mut counter = LexCounter::new();
        counter.decrement("alice");
        assert_eq!(*counter, -1);
    }

    #[test]
    fn it_increments_and_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_counter = LexCounter::new();
        let mut bob_counter = LexCounter::new();

        let alice_deltas = vec![
            alice_counter.increment(&alice_id),
            alice_counter.increment(&alice_id),
        ];

        let bob_deltas = vec![
            bob_counter.decrement(&bob_id),
            bob_counter.decrement(&bob_id),
            bob_counter.decrement(&bob_id),
        ];

        for delta in alice_deltas.into_iter() {
            bob_counter.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_counter.update(delta);
        }

        assert_eq!(*alice_counter, -1);
        assert_eq!(*bob_counter, -1);
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_range in 0usize..1000,
            bob_range in 0usize..1000,
            alice_modulo in 3usize..10,
            bob_modulo in 3usize..10
        ) {
            let alice_id = "alice".to_string();
            let bob_id = "bob".to_string();
            let mut alice_counter = LexCounter::new();
            let mut bob_counter = LexCounter::new();

            let alice_deltas = (0..alice_range).map(|count| match count % alice_modulo {
                0 => alice_counter.increment(&alice_id),
                _ => alice_counter.decrement(&alice_id)
            }).collect::<Vec<LexCounterState<&String>>>();

            let bob_deltas = (0..bob_range).map(|count| match count % bob_modulo {
                0 => bob_counter.increment(&bob_id),
                _ => bob_counter.decrement(&bob_id)
            }).collect::<Vec<LexCounterState<&String>>>();

            for delta in alice_deltas.into_iter() {
                bob_counter.update(delta);
            }

            for delta in bob_deltas.into_iter() {
                alice_counter.update(delta);
            }

            assert_eq!(*alice_counter, *bob_counter);
        }
    }
}
