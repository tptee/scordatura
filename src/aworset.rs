use crate::crdt::DeltaCrdt;
use crate::internal::dotset::DotSet;
use std::collections::BTreeSet;
use std::fmt::Debug;
use std::{iter::FromIterator, ops::Deref};

#[derive(Debug)]
pub struct AwOrSet<K: Ord + Clone, V: Eq + Ord + Clone> {
    state: DotSet<K, V>,
    value: BTreeSet<V>,
}

impl<
        K: std::fmt::Display + std::fmt::Debug + Ord + Clone,
        V: std::fmt::Debug + Eq + Ord + Clone,
    > AwOrSet<K, V>
{
    pub fn new() -> AwOrSet<K, V> {
        AwOrSet {
            state: DotSet::new(),
            value: BTreeSet::new(),
        }
    }

    pub fn add(&mut self, id: K, value: V) -> DotSet<K, V> {
        println!("replica {} added value {:?}", id, value);
        dbg!("dots in self before update", &self.state);
        let mut remove_delta = self.state.remove_value(value.clone());
        dbg!("remove_value delta", &remove_delta);
        let add_delta = self.state.add(id, value);
        dbg!("add delta", &remove_delta);

        self.update(remove_delta.clone());
        self.update(add_delta.clone());
        self.derive();

        remove_delta.join(self.state.clone());
        dbg!("final delta", &remove_delta);
        dbg!("dots in self", &self.state);
        remove_delta
    }

    pub fn remove(&mut self, value: V) -> DotSet<K, V> {
        println!("replica removed value {:?}", value);
        dbg!("dots in self before update", &self.state);
        let remove_delta = self.state.remove_value(value.clone());
        self.update(remove_delta.clone());
        self.derive();

        let mut final_remove_delta = self.state.clone();
        final_remove_delta.join(remove_delta);
        // let remove_delta.join(self.state.clone());
        dbg!("final delta", &final_remove_delta);
        dbg!("dots in self", &self.state);
        final_remove_delta
        // self.state.clone()
        // remove_delta
    }
}

impl<K: std::fmt::Debug + Ord + Clone, V: std::fmt::Debug + Eq + Ord + Clone> DeltaCrdt
    for AwOrSet<K, V>
{
    type State = DotSet<K, V>;

    fn update(&mut self, theirs: Self::State) {
        // dbg!(self.state.clone(), theirs.clone());
        self.state.join(theirs);
        // dbg!(&self.state.values());
        self.derive();
    }

    fn derive(&mut self) {
        self.value = self.state.values().cloned().collect();
        // self.value.clear();
        // for item in self.state.values() {
        //     self.value.insert(item.clone());
        // }
    }
}

impl<K: Ord + Clone, V: Eq + Ord + Clone> Deref for AwOrSet<K, V> {
    type Target = BTreeSet<V>;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_adds_removes_and_dedupes() {
        let mut set = AwOrSet::new();
        set.add("alice", "a");
        assert_eq!(*set, BTreeSet::from_iter(vec!["a"]));
        set.remove("a");
        assert_eq!((*set).len(), 0);
        set.add("alice", "AAA");
        set.add("alice", "AAA");
        assert_eq!(*set, BTreeSet::from_iter(vec!["AAA"]));
        set.remove("AAA");
        assert_eq!((*set).len(), 0);
    }

    #[test]
    fn it_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_set = AwOrSet::new();
        let mut bob_set = AwOrSet::new();

        let alice_deltas = vec![
            alice_set.add(&alice_id, "a"),
            alice_set.add(&alice_id, "b"),
            alice_set.remove("b"),
            alice_set.add(&alice_id, "c"),
        ];
        assert_eq!(*alice_set, BTreeSet::from_iter(vec!["a", "c"]));

        let bob_deltas = vec![
            bob_set.add(&bob_id, "a"),
            bob_set.remove("a"),
            bob_set.add(&bob_id, "b"),
            bob_set.add(&bob_id, "d"),
            bob_set.add(&bob_id, "e"),
        ];
        assert_eq!(*bob_set, BTreeSet::from_iter(vec!["b", "d", "e"]));

        dbg!("alice", &alice_deltas);
        dbg!("bob", &bob_deltas);

        for delta in alice_deltas.into_iter() {
            bob_set.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_set.update(delta);
        }

        assert_eq!(
            *alice_set,
            BTreeSet::from_iter(vec!["a", "b", "c", "d", "e"])
        );
        assert_eq!(*bob_set, BTreeSet::from_iter(vec!["a", "b", "c", "d", "e"]));
    }

    // proptest! {
    //     #[test]
    //     fn it_converges_between_replicas_arbitrary(
    //         alice_range in 0usize..1000,
    //         bob_range in 0usize..1000,
    //         alice_modulo in 3usize..10,
    //         bob_modulo in 3usize..10
    //     ) {
    //         let alice_id = "alice";
    //         let bob_id = "bob";
    //         let mut alice_set = AwOrSet::new();
    //         let mut bob_set = AwOrSet::new();

    //         let alice_deltas = (0..alice_range).map(|count| match count % alice_modulo {
    //             0 => alice_set.write(&alice_id, "hello"),
    //             _ => alice_set.write(&alice_id, "world")
    //         }).collect::<Vec<DotSet<_, _>>>();

    //         let bob_deltas = (0..bob_range).map(|count| match count % bob_modulo {
    //             0 => bob_set.write(&bob_id, "world"),
    //             _ => bob_set.write(&bob_id, "hello")
    //         }).collect::<Vec<DotSet<_, _>>>();

    //         for delta in alice_deltas.into_iter() {
    //             bob_set.update(delta);
    //         }

    //         for delta in bob_deltas.into_iter() {
    //             alice_set.update(delta);
    //         }

    //         assert_eq!(*alice_set, *bob_set);
    //     }
    // }
}
