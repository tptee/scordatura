use crate::crdt::DeltaCrdt;
use crate::internal::dotset::DotSet;
use std::fmt::Debug;
use std::ops::Deref;

pub struct EwFlag<K: Ord + Clone + Debug> {
    state: DotSet<K, bool>,
    value: bool,
}

impl<K: Ord + Clone + Debug> EwFlag<K> {
    pub fn new() -> EwFlag<K> {
        EwFlag {
            state: DotSet::new(),
            value: false,
        }
    }

    pub fn enable(&mut self, id: K) -> DotSet<K, bool> {
        let mut remove_delta = self.state.remove_value(true);
        let add_delta = self.state.add(id, true);

        self.update(remove_delta.clone());
        self.update(add_delta.clone());

        remove_delta.join(add_delta);
        remove_delta
    }

    pub fn disable(&mut self) -> DotSet<K, bool> {
        let remove_delta = self.state.remove_value(true);
        self.update(remove_delta.clone());
        remove_delta
    }
}

impl<K: Ord + Clone + Debug> DeltaCrdt for EwFlag<K> {
    type State = DotSet<K, bool>;

    fn update(&mut self, theirs: Self::State) {
        self.state.join(theirs);

        self.derive();
    }

    fn derive(&mut self) {
        self.value = !self.state.is_bottom();
    }
}

impl<K: Ord + Clone + Debug> Deref for EwFlag<K> {
    type Target = bool;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_toggles() {
        let mut flag = EwFlag::new();
        assert_eq!(*flag, false);
        flag.enable("yeet");
        assert_eq!(*flag, true);
        flag.disable();
        assert_eq!(*flag, false);
        flag.enable("yeet");
        assert_eq!(*flag, true);
    }

    #[test]
    fn it_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_flag = EwFlag::new();
        let mut bob_flag = EwFlag::new();

        let alice_deltas = vec![alice_flag.enable(&alice_id), alice_flag.disable()];
        let bob_deltas = vec![bob_flag.disable(), bob_flag.enable(&bob_id)];

        for delta in alice_deltas.into_iter() {
            bob_flag.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_flag.update(delta);
        }

        assert_eq!(*alice_flag, true);
        assert_eq!(*bob_flag, true);
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_range in 0usize..1000,
            bob_range in 0usize..1000,
            alice_modulo in 3usize..10,
            bob_modulo in 3usize..10
        ) {
            let alice_id = "alice";
            let bob_id = "bob";
            let mut alice_flag = EwFlag::new();
            let mut bob_flag = EwFlag::new();

            let alice_deltas = (0..alice_range).map(|count| match count % alice_modulo {
                0 => alice_flag.enable(&alice_id),
                _ => alice_flag.disable()
            }).collect::<Vec<DotSet<_, bool>>>();

            let bob_deltas = (0..bob_range).map(|count| match count % bob_modulo {
                0 => bob_flag.disable(),
                _ => bob_flag.enable(&bob_id)
            }).collect::<Vec<DotSet<_, bool>>>();


            for delta in alice_deltas.into_iter() {
                bob_flag.update(delta);
            }

            for delta in bob_deltas.into_iter() {
                alice_flag.update(delta);
            }

            assert_eq!(*alice_flag, *bob_flag);
        }
    }
}
