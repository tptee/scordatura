#![deny(clippy::all, clippy::pedantic)]

pub mod crdt;

// Data structures with no ID requirements
pub mod gset;
pub mod rwlwwset;
pub mod twopset;

// Data structures that require replica IDs
pub mod gcounter;
pub mod lexcounter;
pub mod pncounter;

// Causal data structures (dot-based)
pub mod aworset;
pub mod dwflag;
pub mod ewflag;
pub mod mvregister;

mod internal;
