use super::crdt::DeltaCrdt;
use std::collections::BTreeSet;
use std::ops::Deref;

pub type GSetState<T> = BTreeSet<T>;

pub struct GSet<T> {
    pub(crate) state: GSetState<T>,
}

impl<T: Ord> GSet<T> {
    pub fn new() -> Self {
        GSet {
            state: BTreeSet::new(),
        }
    }
}

impl<T: Ord + Clone> GSet<T> {
    pub fn add(&mut self, value: T) -> GSetState<T> {
        self.state.insert(value.clone());

        let mut delta = BTreeSet::new();
        delta.insert(value);
        delta
    }
}

impl<T: Ord> DeltaCrdt for GSet<T> {
    type State = GSetState<T>;

    fn update(&mut self, theirs: Self::State) {
        for element in theirs {
            self.state.insert(element);
        }
    }

    fn derive(&mut self) {}
}

impl<T> Deref for GSet<T> {
    type Target = BTreeSet<T>;

    fn deref(&self) -> &Self::Target {
        &self.state
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use std::iter::FromIterator;

    #[test]
    fn it_converges_between_replicas() {
        let mut alice_set = GSet::new();
        let alice_elements = vec![3, 2, 1];
        let mut bob_set = GSet::new();
        let bob_elements = vec![5, 4, 3];

        for element in alice_elements {
            let delta = alice_set.add(element);
            bob_set.update(delta);
        }

        for element in bob_elements {
            let delta = bob_set.add(element);
            alice_set.update(delta);
        }

        assert_eq!(*alice_set, *bob_set);
    }

    proptest! {
        #[test]
        fn it_can_add_elements(elements in prop::collection::vec(any::<i64>(), 1usize..1000)) {
            let mut set = GSet::new();

            for element in elements.clone() {
                set.add(element);
            }

            prop_assert_eq!(&*set, &BTreeSet::from_iter(elements));
        }

        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_elements in prop::collection::vec(any::<i64>(), 1usize..1000),
            bob_elements in prop::collection::vec(any::<i64>(), 1usize..1000)
        ) {
            let mut alice_set = GSet::new();
            let mut bob_set = GSet::new();

            for element in alice_elements {
                let delta = alice_set.add(element);
                bob_set.update(delta);
            }

            for element in bob_elements {
                let delta = bob_set.add(element);
                alice_set.update(delta);
            }

            assert_eq!(*alice_set, *bob_set);
        }
    }
}
