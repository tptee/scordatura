use super::crdt::DeltaCrdt;
use super::gcounter::{GCounter, GCounterState};
use std::ops::Deref;

pub struct PNCounterState {
    positive: GCounterState,
    negative: GCounterState,
}

pub struct PNCounter {
    positive: GCounter,
    negative: GCounter,
    value: i64,
}

impl PNCounter {
    pub fn new() -> Self {
        PNCounter {
            positive: GCounter::new(),
            negative: GCounter::new(),
            value: 0i64,
        }
    }

    pub fn increment<T: AsRef<str> + Ord>(&mut self, id: T) -> PNCounterState {
        let delta = self.positive.increment(id);

        self.derive();

        PNCounterState {
            positive: delta,
            negative: self.negative.state.clone(),
        }
    }

    pub fn decrement<T: AsRef<str> + Ord>(&mut self, id: T) -> PNCounterState {
        let delta = self.negative.increment(id);

        self.derive();

        PNCounterState {
            positive: self.positive.state.clone(),
            negative: delta,
        }
    }
}

impl DeltaCrdt for PNCounter {
    type State = PNCounterState;

    fn update(&mut self, theirs: Self::State) {
        self.positive.update(theirs.positive);
        self.negative.update(theirs.negative);

        self.derive();
    }

    fn derive(&mut self) {
        self.value = self.positive.value - self.negative.value;
    }
}

impl Deref for PNCounter {
    type Target = i64;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_starts_with_zero() {
        let counter = PNCounter::new();
        assert_eq!(*counter, 0)
    }

    #[test]
    fn it_increments() {
        let mut counter = PNCounter::new();
        counter.increment("alice");
        assert_eq!(*counter, 1);
    }

    #[test]
    fn it_decrements() {
        let mut counter = PNCounter::new();
        counter.decrement("alice");
        assert_eq!(*counter, -1);
    }

    #[test]
    fn it_increments_and_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_counter = PNCounter::new();
        let mut bob_counter = PNCounter::new();

        let alice_deltas = vec![
            alice_counter.increment(&alice_id),
            alice_counter.increment(&alice_id),
        ];

        let bob_deltas = vec![
            bob_counter.decrement(&bob_id),
            bob_counter.decrement(&bob_id),
            bob_counter.decrement(&bob_id),
        ];

        for delta in alice_deltas.into_iter() {
            bob_counter.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_counter.update(delta);
        }

        assert_eq!(*alice_counter, -1);
        assert_eq!(*bob_counter, -1);
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_range in 0usize..1000,
            bob_range in 0usize..1000,
            alice_modulo in 3usize..10,
            bob_modulo in 3usize..10
        ) {
            let alice_id = "alice";
            let bob_id = "bob";
            let mut alice_counter = PNCounter::new();
            let mut bob_counter = PNCounter::new();

            let alice_deltas = (0..alice_range).map(|count| match count % alice_modulo {
                0 => alice_counter.increment(&alice_id),
                _ => alice_counter.decrement(&alice_id)
            }).collect::<Vec<PNCounterState>>();

            let bob_deltas = (0..bob_range).map(|count| match count % bob_modulo {
                0 => bob_counter.increment(&bob_id),
                _ => bob_counter.decrement(&bob_id)
            }).collect::<Vec<PNCounterState>>();


            for delta in alice_deltas.into_iter() {
                bob_counter.update(delta);
            }

            for delta in bob_deltas.into_iter() {
                alice_counter.update(delta);
            }

            assert_eq!(*alice_counter, *bob_counter);
        }
    }
}
