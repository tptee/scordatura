use crate::crdt::DeltaCrdt;
use crate::internal::dotset::DotSet;
use std::collections::BTreeSet;
use std::fmt::Debug;
use std::{iter::FromIterator, ops::Deref};

#[derive(Debug)]
pub struct MvRegister<K: Ord + Clone + Debug, V: Eq + Ord + Clone + Debug> {
    state: DotSet<K, V>,
    value: BTreeSet<V>,
}

impl<K: Ord + Clone + Debug, V: Eq + Ord + Clone + Debug> MvRegister<K, V> {
    pub fn new() -> MvRegister<K, V> {
        MvRegister {
            state: DotSet::new(),
            value: BTreeSet::new(),
        }
    }

    pub fn write(&mut self, id: K, value: V) -> DotSet<K, V> {
        let mut remove_delta = self.state.remove_all();
        let add_delta = self.state.add(id, value);

        self.update(remove_delta.clone());
        self.update(add_delta.clone());

        remove_delta.join(add_delta);
        remove_delta
    }
}

impl<K: Ord + Clone + Debug, V: Eq + Ord + Clone + Debug> DeltaCrdt for MvRegister<K, V> {
    type State = DotSet<K, V>;

    fn update(&mut self, theirs: Self::State) {
        self.state.join(theirs);

        self.derive();
    }

    fn derive(&mut self) {
        self.value.clear();
        for item in self.state.values() {
            self.value.insert(item.clone());
        }
    }
}

impl<K: Ord + Clone + Debug, V: Eq + Ord + Clone + Debug> Deref for MvRegister<K, V> {
    type Target = BTreeSet<V>;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_writes() {
        let mut register = MvRegister::new();
        register.write("alice", "a");
        assert_eq!(*register, BTreeSet::from_iter(vec!["a"]));
        register.write("alice", "b");
        assert_eq!(*register, BTreeSet::from_iter(vec!["b"]));
    }

    #[test]
    fn it_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_register = MvRegister::new();
        let mut bob_register = MvRegister::new();

        let alice_deltas = vec![
            alice_register.write(&alice_id, "hello"),
            alice_register.write(&alice_id, "world"),
        ];
        let bob_deltas = vec![
            bob_register.write(&bob_id, "world"),
            bob_register.write(&bob_id, "hello"),
        ];

        for delta in alice_deltas.into_iter() {
            bob_register.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_register.update(delta);
        }

        assert_eq!(*alice_register, BTreeSet::from_iter(vec!["hello", "world"]));
        assert_eq!(*bob_register, BTreeSet::from_iter(vec!["hello", "world"]));

        let bob_resolution = bob_register.write(&bob_id, "hello world");
        alice_register.update(bob_resolution);

        assert_eq!(*alice_register, BTreeSet::from_iter(vec!["hello world"]));
        assert_eq!(*bob_register, BTreeSet::from_iter(vec!["hello world"]));
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_range in 0usize..1000,
            bob_range in 0usize..1000,
            alice_modulo in 3usize..10,
            bob_modulo in 3usize..10
        ) {
            let alice_id = "alice";
            let bob_id = "bob";
            let mut alice_register = MvRegister::new();
            let mut bob_register = MvRegister::new();

            let alice_deltas = (0..alice_range).map(|count| match count % alice_modulo {
                0 => alice_register.write(&alice_id, "hello"),
                _ => alice_register.write(&alice_id, "world")
            }).collect::<Vec<DotSet<_, _>>>();

            let bob_deltas = (0..bob_range).map(|count| match count % bob_modulo {
                0 => bob_register.write(&bob_id, "world"),
                _ => bob_register.write(&bob_id, "hello")
            }).collect::<Vec<DotSet<_, _>>>();

            for delta in alice_deltas.into_iter() {
                bob_register.update(delta);
            }

            for delta in bob_deltas.into_iter() {
                alice_register.update(delta);
            }

            assert_eq!(*alice_register, *bob_register);
        }
    }
}
