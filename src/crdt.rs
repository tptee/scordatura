pub trait DeltaCrdt {
    type State;

    fn update(&mut self, theirs: Self::State);
    fn derive(&mut self);
}
