use super::crdt::DeltaCrdt;
use std::cmp;
use std::collections::{BTreeMap, BTreeSet};
use std::iter::FromIterator;
use std::ops::Deref;

pub type GCounterState = BTreeMap<String, i64>;

pub struct GCounter {
    pub(crate) state: GCounterState,
    pub(crate) value: i64,
}

impl GCounter {
    pub fn new() -> Self {
        GCounter {
            state: BTreeMap::new(),
            value: 0i64,
        }
    }

    pub fn increment<T: AsRef<str> + Ord>(&mut self, id: T) -> GCounterState {
        let id = id.as_ref();
        let current = self.state.get(id).unwrap_or(&0);
        let new = current + 1;

        self.state.insert(String::from(id), new);
        self.derive();

        let mut delta = BTreeMap::new();
        delta.insert(String::from(id), new);
        delta
    }
}

impl<'a> DeltaCrdt for GCounter {
    type State = GCounterState;

    fn update(&mut self, theirs: Self::State) {
        let our_keys = BTreeSet::from_iter(self.state.keys());
        let their_keys = BTreeSet::from_iter(theirs.keys());
        let keys = our_keys.union(&their_keys);

        let mut new_state = BTreeMap::new();
        for key in keys {
            let key = key.to_string();
            let our_counter = self.state.get(&key).unwrap_or(&0);
            let their_counter = theirs.get(&key).unwrap_or(&0);
            let new_counter = cmp::max(our_counter, their_counter);
            new_state.insert(key, *new_counter);
        }

        self.state = new_state;

        self.derive();
    }

    fn derive(&mut self) {
        self.value = self.state.values().sum();
    }
}

impl Deref for GCounter {
    type Target = i64;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn it_starts_with_zero() {
        let counter = GCounter::new();
        assert_eq!(*counter, 0)
    }

    #[test]
    fn it_increments_and_returns_delta() {
        let mut counter = GCounter::new();
        let delta = counter.increment("alice");
        assert_eq!(delta.get("alice"), Some(&1));
    }

    #[test]
    fn it_increments_and_converges_between_replicas() {
        let alice_id = "alice";
        let bob_id = "bob";
        let mut alice_counter = GCounter::new();
        let mut bob_counter = GCounter::new();

        let alice_deltas = vec![
            alice_counter.increment(&alice_id),
            alice_counter.increment(&alice_id),
        ];

        let bob_deltas = vec![
            bob_counter.increment(&bob_id),
            bob_counter.increment(&bob_id),
        ];

        for delta in alice_deltas.into_iter() {
            bob_counter.update(delta);
        }

        for delta in bob_deltas.into_iter() {
            alice_counter.update(delta);
        }

        assert_eq!(*alice_counter, 4);
        assert_eq!(*bob_counter, 4);
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(alice_range in 0usize..1000, bob_range in 0usize..1000) {
            let alice_id = "alice";
            let bob_id = "bob";
            let mut alice_counter = GCounter::new();
            let mut bob_counter = GCounter::new();

            let alice_deltas = (0..alice_range).map(|_| alice_counter.increment(&alice_id)).collect::<Vec<GCounterState>>();
            let bob_deltas = (0..bob_range).map(|_| bob_counter.increment(&bob_id)).collect::<Vec<GCounterState>>();

            for delta in alice_deltas.into_iter() {
                bob_counter.update(delta);
            }

            for delta in bob_deltas.into_iter() {
                alice_counter.update(delta);
            }

            assert_eq!(*alice_counter, *bob_counter);
        }
    }
}
