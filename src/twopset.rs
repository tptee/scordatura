use super::crdt::DeltaCrdt;
use std::collections::BTreeSet;
use std::ops::Deref;

#[derive(Debug)]
pub struct TwoPSetState<T> {
    adds: BTreeSet<T>,
    removes: BTreeSet<T>,
}

impl<T: Ord> TwoPSetState<T> {
    pub fn new() -> TwoPSetState<T> {
        TwoPSetState {
            adds: BTreeSet::new(),
            removes: BTreeSet::new(),
        }
    }
}

#[derive(Debug)]
pub struct TwoPSet<T> {
    pub(crate) state: TwoPSetState<T>,
    value: BTreeSet<T>,
}

impl<T: Ord> TwoPSet<T> {
    pub fn new() -> Self {
        TwoPSet {
            state: TwoPSetState::new(),
            value: BTreeSet::new(),
        }
    }
}

impl<T: Ord + Clone> TwoPSet<T> {
    pub fn add(&mut self, value: T) -> TwoPSetState<T> {
        // Don't add the value to the set if it's been removed before
        if self.state.removes.contains(&value) {
            return TwoPSetState::new();
        }

        self.state.adds.insert(value.clone());
        self.derive();

        let mut delta = TwoPSetState::new();
        delta.adds.insert(value);
        delta
    }

    pub fn remove(&mut self, value: T) -> TwoPSetState<T> {
        self.state.adds.remove(&value);
        self.state.removes.insert(value.clone());
        self.derive();

        let mut delta = TwoPSetState::new();
        delta.removes.insert(value);
        delta
    }
}

impl<T: Ord + Clone> DeltaCrdt for TwoPSet<T> {
    type State = TwoPSetState<T>;

    // TODO: avoid cloning
    fn update(&mut self, theirs: Self::State) {
        let adds: BTreeSet<T> = self.state.adds.union(&theirs.adds).cloned().collect();
        let removes: BTreeSet<T> = self.state.removes.union(&theirs.removes).cloned().collect();

        self.state.adds = adds.difference(&removes).cloned().collect();
        self.state.removes = removes;

        self.derive();
    }

    fn derive(&mut self) {
        self.value = self.state.adds.clone();
    }
}

impl<T> Deref for TwoPSet<T> {
    type Target = BTreeSet<T>;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use std::iter::FromIterator;

    #[test]
    fn it_converges_between_replicas() {
        let mut alice_set = TwoPSet::new();
        let alice_elements = vec![3, 2, 1];
        let mut bob_set = TwoPSet::new();
        let bob_elements = vec![5, 4, 3];

        for element in alice_elements {
            let delta = alice_set.add(element);
            bob_set.update(delta);
        }

        for element in bob_elements {
            let delta = bob_set.add(element);
            alice_set.update(delta);
        }

        // Bob removes 3 from the set and sends the change to Alice
        alice_set.update(bob_set.remove(3));

        assert_eq!(*alice_set, *bob_set);
    }

    proptest! {
        #[test]
        fn it_can_add_and_remove_elements(
            adds in prop::collection::vec(any::<i64>(), 1usize..1000),
        ) {
            let mut set = TwoPSet::new();

            for add in adds.clone() {
                set.add(add);
            }

            for remove in adds.clone().into_iter().take(10) {
                set.remove(remove);
            }

            prop_assert_eq!(&*set, &BTreeSet::from_iter(adds.into_iter().skip(10)));
        }

        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_elements in prop::collection::vec(any::<i64>(), 1usize..100),
            bob_elements in prop::collection::vec(any::<i64>(), 1usize..100)
        ) {
            let mut alice_set = TwoPSet::new();
            let mut bob_set = TwoPSet::new();

            for element in alice_elements {
                let delta = alice_set.add(element);
                bob_set.update(delta);
            }

            for element in bob_elements.clone() {
                let delta = bob_set.add(element);
                alice_set.update(delta);
            }

            for element in bob_elements.into_iter().take(10) {
                let delta = bob_set.remove(element);
                alice_set.update(delta);
            }

            prop_assert_eq!(&*alice_set, &*bob_set);
        }
    }
}
