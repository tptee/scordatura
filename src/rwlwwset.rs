use super::crdt::DeltaCrdt;
use super::internal::lex::LexPair;
use std::collections::{BTreeSet, HashMap};
use std::hash::Hash;
use std::iter::FromIterator;
use std::ops::Deref;

type RwLwwSetState<KV, T> = HashMap<KV, LexPair<KV, T>>;

#[derive(Clone)]
pub struct RwLwwSet<KV, T: Ord> {
    pub(crate) state: RwLwwSetState<KV, T>,
    value: BTreeSet<KV>,
}

impl<KV: Hash + Eq + Ord + Clone, T: Ord + Clone> RwLwwSet<KV, T> {
    pub fn new() -> Self {
        RwLwwSet {
            state: RwLwwSetState::new(),
            value: BTreeSet::new(),
        }
    }

    pub fn add(&mut self, value: KV, timestamp: T) -> RwLwwSetState<KV, T> {
        self.add_or_remove(value, timestamp, true)
    }

    pub fn remove(&mut self, value: KV, timestamp: T) -> RwLwwSetState<KV, T> {
        self.add_or_remove(value, timestamp, false)
    }

    fn add_or_remove(
        &mut self,
        value: KV,
        timestamp: T,
        is_addition: bool,
    ) -> RwLwwSetState<KV, T> {
        let pair = LexPair::new(value.clone(), timestamp, is_addition);
        let mut delta = HashMap::new();
        delta.insert(value, pair);

        self.update(delta.clone());

        delta
    }
}

impl<KV: Hash + Eq + Clone + Ord, T: Ord> DeltaCrdt for RwLwwSet<KV, T> {
    type State = RwLwwSetState<KV, T>;

    fn update(&mut self, mut theirs: Self::State) {
        let keys: BTreeSet<_> =
            BTreeSet::from_iter(self.state.keys().chain(theirs.keys()).cloned());

        for key in keys {
            let needs_lex_join = self.state.contains_key(&key) && theirs.contains_key(&key);

            if !needs_lex_join {
                let value = self.state.remove(&key).or_else(|| theirs.remove(&key));
                if let Some(v) = value {
                    self.state.insert(key, v);
                }
                continue;
            }

            if let Some(value) = LexPair::join(
                self.state.remove(&key),
                theirs.remove(&key),
                |ours, theirs| match (ours.is_addition, theirs.is_addition) {
                    (false, true) => ours,
                    (true, false) => theirs,
                    _ => {
                        if ours.value < theirs.value {
                            ours
                        } else {
                            theirs
                        }
                    }
                },
            ) {
                self.state.insert(key, value);
            }
        }

        self.derive();
    }

    fn derive(&mut self) {
        self.value.clear();
        for item in self.state.values() {
            if item.is_addition {
                self.value.insert(item.value.clone());
            }
        }
    }
}

impl<KV: Hash + Eq, T: Ord> Deref for RwLwwSet<KV, T> {
    type Target = BTreeSet<KV>;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use std::iter::FromIterator;

    #[test]
    fn it_adds_removes_and_dedupes() {
        let mut set = RwLwwSet::new();

        set.add("a".to_string(), 1);
        assert_eq!(*set, BTreeSet::from_iter(vec!["a".to_string()]));

        set.remove("a".to_string(), 2);
        assert_eq!(*set, BTreeSet::from_iter(vec![]));

        set.add("aaa".to_string(), 3);
        set.add("aaa".to_string(), 4);
        assert_eq!(*set, BTreeSet::from_iter(vec!["aaa".to_string()]));

        set.remove("aaa".to_string(), 5);
        assert_eq!(*set, BTreeSet::from_iter(vec![]));
    }

    #[test]
    fn it_converges_between_replicas() {
        let mut alice_set = RwLwwSet::new();
        let mut bob_set = RwLwwSet::new();

        let alice_elements = vec![
            ("a".to_string(), 1, true),
            ("b".to_string(), 2, true),
            ("b".to_string(), 3, false),
            ("c".to_string(), 4, true),
        ];

        let bob_elements = vec![
            ("a".to_string(), 1, true),
            ("a".to_string(), 2, false),
            ("b".to_string(), 3, true),
            ("d".to_string(), 4, true),
            ("e".to_string(), 5, true),
        ];

        for (value, timestamp, is_addition) in alice_elements {
            let delta = if is_addition {
                alice_set.add(value, timestamp)
            } else {
                alice_set.remove(value, timestamp)
            };
            bob_set.update(delta);
        }

        for (value, timestamp, is_addition) in bob_elements {
            let delta = if is_addition {
                bob_set.add(value, timestamp)
            } else {
                bob_set.remove(value, timestamp)
            };
            alice_set.update(delta);
        }

        assert_eq!(*alice_set, *bob_set);
    }

    proptest! {
        #[test]
        fn it_converges_between_replicas_arbitrary(
            alice_elements in prop::collection::vec((any::<i64>(), any::<bool>()), 1usize..100),
            bob_elements in prop::collection::vec((any::<i64>(), any::<bool>()), 1usize..100),
        ) {
            let mut alice_set = RwLwwSet::new();
            let mut bob_set = RwLwwSet::new();

            for (i, (value, is_addition)) in alice_elements.into_iter().enumerate() {
                let delta = if is_addition {
                    alice_set.add(value, i + 1)
                } else {
                    alice_set.remove(value, i + 1)
                };
                bob_set.update(delta);
            }

            for (i, (value, is_addition)) in bob_elements.into_iter().enumerate() {
                let delta = if is_addition {
                    bob_set.add(value, i + 1)
                } else {
                    bob_set.remove(value, i + 1)
                };
                alice_set.update(delta);
            }

            assert_eq!(*alice_set, *bob_set);
        }
    }
}
