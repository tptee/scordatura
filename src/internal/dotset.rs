use crate::internal::causalcontext::{CausalContext, Dot};
use crate::internal::dotstore::DotStore;
use std::collections::{btree_map::Values, BTreeMap, BTreeSet};
use std::fmt::Debug;
use std::iter::FromIterator;

#[derive(Clone, Debug)]
pub struct DotSet<K: Ord + Clone, V: Eq> {
    pub(crate) causal_context: CausalContext<K>,
    pub(crate) dots: BTreeMap<Dot<K>, V>,
}

impl<K: Ord + Clone + Debug, V: Eq + Clone + Debug> DotStore<K, V> for DotSet<K, V> {
    fn dots(&self) -> BTreeSet<Dot<K>> {
        BTreeSet::from_iter(self.dots.keys().cloned())
    }

    fn set_causal_context(&mut self, new: CausalContext<K>) {
        self.causal_context = new;
    }
}

impl<K: std::fmt::Debug + Ord + Clone, V: std::fmt::Debug + Eq + Clone> DotSet<K, V> {
    pub fn new() -> DotSet<K, V> {
        DotSet {
            causal_context: CausalContext::new(),
            dots: BTreeMap::new(),
        }
    }

    pub fn with_causal_context(mut self, causal_context: CausalContext<K>) -> Self {
        self.causal_context = causal_context;
        self
    }

    pub fn with_dots(mut self, dots: BTreeMap<Dot<K>, V>) -> Self {
        self.dots = dots;
        self
    }

    pub fn is_bottom(&self) -> bool {
        self.dots.len() == 0
    }

    pub fn add(&mut self, id: K, value: V) -> DotSet<K, V> {
        let dot = self.causal_context.make_dot(id);
        self.dots.insert(dot.clone(), value.clone());

        let mut delta_dots = BTreeMap::new();
        let mut delta_causal_context = CausalContext::new();

        delta_dots.insert(dot.clone(), value);
        delta_causal_context.insert_dot(dot, false);

        DotSet::new()
            .with_causal_context(delta_causal_context)
            .with_dots(delta_dots)
    }

    pub fn add_without_delta(&mut self, id: K, value: V) {
        let dot = self.causal_context.make_dot(id);
        self.dots.insert(dot.clone(), value.clone());
    }

    pub fn remove_value(&mut self, value: V) -> DotSet<K, V> {
        let mut delta_dot_set = DotSet::new();

        let result = self
            .dots
            .iter_mut()
            .find(|(_, dot_value)| dot_value == &&value);

        if let Some(result) = result {
            let dot = result.0.clone();
            delta_dot_set.causal_context.insert_dot(dot.clone(), false);
            self.dots.remove(&dot);
        }

        delta_dot_set.causal_context.compact();

        delta_dot_set
    }

    pub fn remove_dot(&mut self, dot: Dot<K>) -> DotSet<K, V> {
        let mut delta_dot_set = DotSet::new();

        if self.dots.contains_key(&dot) {
            delta_dot_set.causal_context.insert_dot(dot.clone(), false);
            self.dots.remove(&dot);
        }

        delta_dot_set.causal_context.compact();

        delta_dot_set
    }

    pub fn remove_all(&mut self) -> DotSet<K, V> {
        let mut delta_dot_set = DotSet::new();

        for (dot, _) in &self.dots {
            delta_dot_set.causal_context.insert_dot(dot.clone(), false)
        }

        // Clear all values, but retain the causal context!
        self.dots = BTreeMap::new();

        delta_dot_set.causal_context.compact();

        delta_dot_set
    }

    pub fn join(&mut self, theirs: DotSet<K, V>) {
        let dots: BTreeSet<_> =
            BTreeSet::from_iter(self.dots.keys().chain(theirs.dots.keys()).cloned());

        for dot in dots {
            // let dot = dbg!(dot);
            // They deleted a value
            if !theirs.dots.contains_key(&dot)
                && self.dots.contains_key(&dot)
                && theirs.causal_context.dot_in(&dot)
            {
                self.dots.remove(&dot);
                continue;
            }

            // They added a value
            if !self.dots.contains_key(&dot) && !self.causal_context.dot_in(&dot) {
                if let Some(their_value) = theirs.dots.get(&dot) {
                    self.dots.insert(dot.clone(), their_value.clone());
                }
                continue;
            }

            println!("holy fuck this aint right: {:#?}", dot);
            // Nothing to do if both have the value!
            // TODO: check why js-delta-crdts has a `joinValues` parameter here:
            // https://github.com/peer-base/js-delta-crdts/blob/master/src/dot-set.js#L119
        }

        self.causal_context.join(theirs.causal_context);
        // let _ = dbg!(&self.dots);
    }

    pub fn values(&mut self) -> Values<Dot<K>, V> {
        self.dots.values()
    }
}
