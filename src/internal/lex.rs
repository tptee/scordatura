use std::cmp::Ordering;

#[derive(Debug, Clone)]
pub struct LexPair<KV, T: Ord> {
    pub value: KV,
    pub timestamp: T,
    pub(crate) is_addition: bool,
}

impl<KV, T: Ord> LexPair<KV, T> {
    pub fn new(value: KV, timestamp: T, is_addition: bool) -> LexPair<KV, T> {
        LexPair {
            value,
            timestamp,
            is_addition,
        }
    }

    pub fn join<J: Fn(LexPair<KV, T>, LexPair<KV, T>) -> LexPair<KV, T>>(
        ours: Option<LexPair<KV, T>>,
        theirs: Option<LexPair<KV, T>>,
        join: J,
    ) -> Option<LexPair<KV, T>> {
        match (ours, theirs) {
            (None, None) => None,
            (Some(ours), None) => Some(ours),
            (None, Some(theirs)) => Some(theirs),
            (Some(ours), Some(theirs)) => match ours.timestamp.cmp(&theirs.timestamp) {
                Ordering::Greater => Some(ours),
                Ordering::Less => Some(theirs),
                Ordering::Equal => Some(join(ours, theirs)),
            },
        }
    }
}
