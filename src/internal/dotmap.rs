// TODO: this shit sucks!

use crate::internal::causalcontext::{CausalContext, Dot};
use crate::internal::dotstore::{DotStore, DotStoreEnum};
use std::collections::{BTreeMap, BTreeSet};
use std::fmt::Debug;
use std::iter::FromIterator;

#[derive(Debug, Clone)]
pub struct DotMap<K: Ord + Clone, V: Eq + Clone> {
    causal_context: CausalContext<K>,
    state: BTreeMap<Dot<K>, DotStoreEnum<K, V>>,
}

impl<K: Ord + Clone + Debug, V: Eq + Clone + Debug> DotStore<K, V> for DotMap<K, V> {
    fn dots(&self) -> BTreeSet<Dot<K>> {
        BTreeSet::from_iter(self.state.values().flat_map(|dot_store| dot_store.dots()))
    }

    fn set_causal_context(&mut self, new: CausalContext<K>) {
        self.causal_context = new;
    }
}

impl<K: Ord + Clone + Debug, V: Eq + Clone + Debug> DotMap<K, V> {
    pub fn new() -> DotMap<K, V> {
        DotMap {
            causal_context: CausalContext::new(),
            state: BTreeMap::new(),
        }
    }

    pub fn with_causal_context(mut self, causal_context: CausalContext<K>) -> Self {
        self.causal_context = causal_context;
        self
    }

    pub fn with_state(mut self, dots: BTreeMap<Dot<K>, DotStoreEnum<K, V>>) -> Self {
        self.state = dots;
        self
    }

    pub fn dots(&self) -> BTreeSet<Dot<K>> {
        BTreeSet::from_iter(self.state.values().flat_map(|dot_store| dot_store.dots()))
    }

    pub fn is_bottom(&self) -> bool {
        self.state.len() == 0
    }

    pub fn compact(&mut self) {
        self.causal_context.compact();
    }

    pub fn join(&mut self, mut theirs: DotMap<K, V>) {
        let keys: BTreeSet<_> =
            BTreeSet::from_iter(self.state.keys().chain(theirs.state.keys()).cloned());

        self.causal_context.join(theirs.causal_context.clone());

        let mut result = BTreeMap::new();

        for key in keys {
            let our_value = self.state.get_mut(&key);
            let their_value = theirs.state.get_mut(&key);

            let new_value = match (our_value, their_value) {
                (Some(mut our_value), Some(mut their_value)) => {
                    our_value.set_causal_context(self.causal_context.clone());
                    their_value.set_causal_context(theirs.causal_context.clone());

                    // TODO: test PartialEq on enum variants to simplify
                    match (&mut our_value, &mut their_value) {
                        (DotStoreEnum::Set(our_value), DotStoreEnum::Set(their_value)) => {
                            our_value.join(their_value.clone());
                        }
                        (DotStoreEnum::Map(our_value), DotStoreEnum::Map(their_value)) => {
                            our_value.join(their_value.clone());
                        }
                        _ => panic!("Tried to join two incompatible child value types!"),
                    }

                    Some(DotStoreEnum::from(our_value.clone()))
                }
                (Some(our_value), None) => Some(our_value.clone()),
                (None, Some(their_value)) => Some(their_value.clone()),
                _ => None,
            };

            if let Some(mut new_value) = new_value {
                new_value.set_causal_context(CausalContext::new());
                result.insert(key, new_value);
            }
        }
    }
}
