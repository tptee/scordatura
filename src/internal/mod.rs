#![deny(clippy::all, clippy::pedantic)]
pub(crate) mod causalcontext;
pub(crate) mod dotmap;
pub(crate) mod dotset;
pub(crate) mod dotstore;
pub(crate) mod lex;
