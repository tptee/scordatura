use std::cmp;
use std::collections::{BTreeMap, BTreeSet};
use std::fmt::Debug;
use std::iter::FromIterator;

#[derive(Ord, PartialOrd, Eq, PartialEq, Clone, Debug)]
pub struct Dot<T: Ord> {
    // Put timestamp first since deriving Ord compares fields in order.
    timestamp: u64,
    id: T,
}

impl<T: Ord> Dot<T> {
    pub fn new(id: T) -> Dot<T> {
        Dot { id, timestamp: 0 }
    }

    pub fn timestamp(mut self, timestamp: u64) -> Dot<T> {
        self.timestamp = timestamp;
        self
    }
}

#[derive(Clone, Debug)]
pub struct CausalContext<T: Ord + Clone> {
    compact_context: BTreeMap<T, u64>,
    dot_cloud: BTreeSet<Dot<T>>,
}

impl<T: Ord + Clone> CausalContext<T> {
    pub fn new() -> CausalContext<T> {
        CausalContext {
            compact_context: BTreeMap::new(),
            dot_cloud: BTreeSet::new(),
        }
    }

    pub fn next(&self, id: T) -> Dot<T> {
        let timestamp = self.compact_context.get(&id).unwrap_or_else(|| &0) + 1;
        Dot::new(id).timestamp(timestamp)
    }

    pub fn make_dot(&mut self, id: T) -> Dot<T> {
        let next_dot = self.next(id);
        self.compact_context
            .insert(next_dot.id.clone(), next_dot.timestamp);
        next_dot
    }

    pub fn dot_in(&self, dot: &Dot<T>) -> bool {
        let timestamp = self.compact_context.get(&dot.id).unwrap_or_else(|| &0);
        dot.timestamp <= *timestamp || self.dot_cloud.contains(&dot)
    }

    pub fn insert_dot(&mut self, dot: Dot<T>, compact: bool) {
        self.dot_cloud.insert(dot);
        if compact {
            self.compact();
        }
    }

    pub fn compact(&mut self) {
        for dot in &self.dot_cloud {
            let existing = self.compact_context.get(&dot.id);
            match existing {
                Some(existing) if &dot.timestamp > existing => {
                    self.compact_context.insert(dot.id.clone(), dot.timestamp);
                }
                _ => {
                    self.compact_context.insert(dot.id.clone(), dot.timestamp);
                }
            }
        }
        self.dot_cloud.clear();
    }

    pub fn join(&mut self, mut theirs: CausalContext<T>) {
        theirs.compact();
        self.compact();

        let keys: BTreeSet<_> = BTreeSet::from_iter(
            self.compact_context
                .keys()
                .chain(theirs.compact_context.keys())
                .cloned(),
        );

        let mut joined_compact_context = BTreeMap::new();
        for key in keys {
            joined_compact_context.insert(
                key.clone(),
                cmp::max(
                    *self.compact_context.get(&key).unwrap_or_else(|| &0),
                    *theirs.compact_context.get(&key).unwrap_or_else(|| &0),
                ),
            );
        }

        self.compact_context = joined_compact_context;
        self.dot_cloud.clear();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_dots_with_compaction() {
        let mut cc = CausalContext::new();

        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(1)), false);

        cc.make_dot("alice");
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(1)), true);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(2)), false);

        cc.insert_dot(Dot::new("alice").timestamp(2), false);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(1)), true);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(2)), true);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(3)), false);

        cc.compact();
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(1)), true);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(2)), true);
        assert_eq!(cc.dot_in(&Dot::new("alice").timestamp(3)), false);
    }

    #[test]
    fn it_converges_replicas() {
        let mut alice_cc = CausalContext::new();
        alice_cc.insert_dot(Dot::new("alice").timestamp(1), false);
        alice_cc.insert_dot(Dot::new("alice").timestamp(2), false);

        let mut bob_cc = CausalContext::new();
        bob_cc.insert_dot(Dot::new("bob").timestamp(1), false);
        bob_cc.insert_dot(Dot::new("bob").timestamp(2), false);
        bob_cc.insert_dot(Dot::new("eve").timestamp(1), false);
        bob_cc.insert_dot(Dot::new("eve").timestamp(2), false);

        alice_cc.join(bob_cc);

        assert_eq!(alice_cc.dot_in(&Dot::new("alice").timestamp(1)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("alice").timestamp(2)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("alice").timestamp(3)), false);

        assert_eq!(alice_cc.dot_in(&Dot::new("bob").timestamp(1)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("bob").timestamp(2)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("bob").timestamp(3)), false);

        assert_eq!(alice_cc.dot_in(&Dot::new("eve").timestamp(1)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("eve").timestamp(2)), true);
        assert_eq!(alice_cc.dot_in(&Dot::new("eve").timestamp(3)), false);
    }
}
