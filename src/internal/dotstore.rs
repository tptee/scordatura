use crate::internal::causalcontext::{CausalContext, Dot};
use crate::internal::dotmap::DotMap;
use crate::internal::dotset::DotSet;
use enum_dispatch::enum_dispatch;
use std::collections::BTreeSet;
use std::fmt::Debug;

#[enum_dispatch]
#[derive(Debug, Clone)]
pub enum DotStoreEnum<K: Ord + Clone, V: Eq + Clone> {
    Set(DotSet<K, V>),
    Map(DotMap<K, V>),
}

#[enum_dispatch(DotStoreEnum)]
pub trait DotStore<K: Ord + Clone + Debug, V: Eq + Clone + Debug> {
    fn dots(&self) -> BTreeSet<Dot<K>>;
    fn set_causal_context(&mut self, new: CausalContext<K>);
}
